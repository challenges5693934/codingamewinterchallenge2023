using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Runtime.Serialization;

public class Solution
{

    /**
     * @param s1 the first line of the page, composed of uppercase letters only
     * @param s2 the second line, composed of uppercase letters and of the same length as s1
     * @return the decrypted message, created by alternating the letters of s1 and s2
     */
    public static string Decrypt(string s1, string s2)
    {
        // Write your code here
        string code = "";
        for (int i = 0; i < s1.Length; i++)
        {
            code += s1[i];
            code += s2[i];
        }

        return code;
    }

    /* Ignore and do not change the code below */

    /**
     * Try a solution
     * @param message the decrypted message, created by alternating the letters of s1 and s2
     */
    private static void TrySolution(string message)
    {
        Console.WriteLine("" + JsonSerializer.Serialize(message));
    }

    public static void Main(string[] args)
    {
        TrySolution(Decrypt(
            JsonSerializer.Deserialize<string>(Console.ReadLine()),
            JsonSerializer.Deserialize<string>(Console.ReadLine())
        ));
    }
    /* Ignore and do not change the code above */
}
