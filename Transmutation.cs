using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Runtime.Serialization;

public class Player
{

    /**
     * @param protonsStart The initial number of protons
     * @param neutronsStart The initial number of neutrons
     * @param protonsTarget The desired number of protons
     * @param neutronsTarget The desired number of neutrons
     */
    public static List<string> Solve(int x1, int y1, int x2, int y2)
    {
        // Write your code here

        List<string> result = new List<string>();

        string p = "PROTON";
        string n = "NEUTRON";
        string a = "ALPHA";

        while (true)
        {
            int targetDiff = x2 - y2;
            int currentDiff = x1 - y1;

            if (targetDiff > currentDiff)
            {
                Proton();
            }
            else if (targetDiff < currentDiff)
            {
                Neutron();

            }
            else
            {
                if (x1 == x2)
                {
                    break;
                }
                else if (x2 > x1)
                {
                    Proton();
                    Neutron();
                }
                else
                {
                    Alpha();
                }
            }
        }

        return result;

        void Proton()
        {
            x1++;
            result.Add(p);
        }
        void Neutron()
        {
            y1++;
            result.Add(n);
        }
        void Alpha()
        {
            x1 -= 2;
            y1 -= 2;
            result.Add(a);
        }

    }

    /* Ignore and do not change the code below */

    /**
     * Try a solution
     */
    private static void TrySolution(List<string> recipe)
    {
        Console.WriteLine("" + JsonSerializer.Serialize(recipe));
    }

    public static void Main(string[] args)
    {
        TrySolution(Solve(
            JsonSerializer.Deserialize<int>(Console.ReadLine()),
            JsonSerializer.Deserialize<int>(Console.ReadLine()),
            JsonSerializer.Deserialize<int>(Console.ReadLine()),
            JsonSerializer.Deserialize<int>(Console.ReadLine())
        ));
    }
    /* Ignore and do not change the code above */
}
}
