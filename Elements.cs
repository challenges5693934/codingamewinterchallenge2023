using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Runtime.Serialization;

public class Player
{

    /**
     * @param grid The initial grid of elements
     * @param rules Transition rules between elements
     */
    public static List<List<int>> Solve(List<List<int>> grid, List<Unknown> rules)
    {

        List<List<int>> result = new List<List<int>>();
        int n = grid.Count;
        for (int i = 0; i < (n - 1); i++)
        {
            result.Add(new List<int>());
        }

        for (int i = 0; i < n - 1; i++)
        {
            for (int j = 0; j < n - 1; j++)
            {
                int transmuted = Traverse((grid[i][j]), (grid[i][j + 1]), (grid[i + 1][j]), (grid[i + 1][j + 1]));

                result[i].Add(transmuted);

            }
        }


        return result;
        int Traverse(int tl, int tr, int bl, int br)
        {
            List<int> pattern = new List<int>() { tl, tr, bl, br };
            for (int i = 0; i < rules.Count; i++)
            {
                var r = rules[i];
                
                // Comparing lists with each other does not mean their elements are checked.
                // this would always return false
                // if (r.Pattern == pattern)
                // {
                //    return r.Result; 
                // }

                bool match = true;

                for (int j = 0; j < 4; j++)
                {
                    if (r.Pattern[j] != pattern[j])
                    {
                        match = false;
                        break;
                    }
                }

                if (match)
                {
                    return r.Result;
                }
            }

            return 0;
        }
    }


    /* Ignore and do not change the code below */

    /**
     * Try a solution
     */
    private static void TrySolution(List<List<int>> newGrid)
    {
        Console.WriteLine("" + JsonSerializer.Serialize(newGrid));
    }

    public static void Main(string[] args)
    {
        TrySolution(Solve(
            JsonSerializer.Deserialize<List<List<int>>>(Console.ReadLine()),
            JsonSerializer.Deserialize<List<Unknown>>(Console.ReadLine())
        ));
    }
    /* Ignore and do not change the code above */
}


public class Unknown
{
    [JsonPropertyName("pattern")]
    public List<int> Pattern { get; set; }
    [JsonPropertyName("result")]
    public int Result { get; set; }
}
